+++
date = 2020-08-27T13:00:00Z
lastmod = 2020-08-27T13:00:00Z
author = "default"
title = "Example Post 4"
subtitle = "Example Post 4. Nouvel ajout de post via command hugo new."
feature = "image/page-default.webp"
+++

Ce post a été ajouté grâce à la commande hugo suivante:

```
hugo new posts/example-post-4.md.
```

### Pourquoi ?

Ce post a _simplement_ pour but de montrer les connaissances du candidat quant à l'ajout d'un post hugo de manière conventionnelle
