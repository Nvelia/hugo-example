const backButton = document.getElementById("back-to-list");
const listEl = document.getElementById("dog-list");
const searchEl = document.getElementById("search");
const suggestionsEl = document.getElementById("suggestions-block");
const searchForm = document.getElementById("search-form");

let onList = true;
let breedList;
let suggestions;

/**
 * Display breed suggestions under the search form
 * @param suggestions array
 */
const checkSuggestions = (suggestions) => {
  suggestionsEl.innerHTML = "";

  if (suggestions) {
    // Displaying suggestion list and filling it
    suggestionsEl.classList.remove("hidden");

    suggestions.forEach((suggestion) => {
      var liEl = document.createElement("li");
      liEl.innerHTML = suggestion;
      liEl.classList.add("breed-list-element");

      suggestionsEl.append(liEl);
    });
  } else {
    clearInput();
  }
};

/**
 * Clears input field and everything related to it
 */
const clearInput = () => {
  suggestions = null;
  suggestionsEl.innerHTML = "";
  suggestionsEl.classList.add("hidden");

  searchEl.value = "";
};

/**
 * Function reseting all settings
 * @param isOnList boolean
 */
const reset = (isOnList) => {
  onList = isOnList;
  listEl.innerHTML = "";
  clearInput();

  // Display or hide back button
  if (onList) {
    backButton.classList.add("hidden");
    return true;
  } else {
    backButton.classList.remove("hidden");
    return false;
  }
};

/**
 * Function getting an image of a breed from an API and a description from an other API
 * @param breed string
 */
const getBreedInfo = (breed) => {
  reset(false);

  // Get a random image of the selected breed
  ajaxCall(`https://dog.ceo/api/breed/${breed}/images/random`).then(
    (response) => {
      var imageEl = document.createElement("img");
      imageEl.src = response.message;
      imageEl.alt = "pictures of " + breed;
      imageEl.classList.add("image-breed-element");

      listEl.append(imageEl);
    }
  );

  // Get a description of the selected breed
  ajaxCall(
    `https://api.thedogapi.com/v1/breeds/search?q=${breed}&75731a8a-a817-452d-8672-eadab7adf9db`
  )
    .then((response) => {
      const descriptionEl = document.createElement("div");
      descriptionEl.classList.add("description-breed-element");

      createDescription(response[0], descriptionEl);
      listEl.append(descriptionEl);
    })
    .catch((err) => console.log(err));
};

/**
 * Creates the HTML content to make the description of the breed
 * @param dog API object
 * @param parentEl HTMLDivElement
 */
const createDescription = (dog, parentEl) => {
  // Dog name block
  var dogName = document.createElement("p");
  dogName.innerHTML = "Name: " + dog.name;

  // Dog size block
  var dogSize = document.createElement("p");
  dogSize.innerHTML = "Size: ~ " + dog.height.metric + "cm";

  // Dog breed group block
  var breedGroup = document.createElement("p");
  breedGroup.innerHTML = "Breed Group: " + dog.breed_group;

  // Dog life span block
  var lifeSpan = document.createElement("p");
  lifeSpan.innerHTML = "Life Span: " + dog.life_span;

  // Dog temperament span block
  var temperament = document.createElement("p");
  temperament.innerHTML = "Temperament: " + dog.temperament;

  // Append created element to parent
  parentEl.append(dogName);
  parentEl.append(dogSize);
  parentEl.append(breedGroup);
  parentEl.append(lifeSpan);
  parentEl.append(temperament);
};

/**
 * Ajax call function
 * @param url string
 */
const ajaxCall = (url) => {
  return $.ajax({
    url: url,
    type: "GET",
    dataType: "json",
  });
};

/**
 * Fetches all the breed from dog API
 */
const getAllBreeds = () => {
  if (location.pathname === "/dogs/") {
    reset(true);

    ajaxCall("https://dog.ceo/api/breeds/list")
      .then((response) => {
        if (!breedList) {
          breedList = response.message;
        }

        // Build the breed list
        breedList.forEach((breed) => {
          var breedEl = document.createElement("div");
          breedEl.innerHTML = breed;

          breedEl.classList.add("breed-element");
          listEl.append(breedEl);
        });
      })
      .catch((err) => console.log(err));
  }
};

// Event Listener:  get breed info on element click
$("body").on("click", ".breed-element, .breed-list-element", function () {
  getBreedInfo(this.innerHTML);
});

// Event Listener:  get user input and suggests breed
$("#search").bind("input", function () {
  let userInput = $(this).val();

  if (userInput === "") {
    suggestions = null;
  } else {
    suggestions = breedList.filter((breed) => {
      if (breed.startsWith(userInput)) {
        return breed;
      }
    });
  }

  checkSuggestions(suggestions);
});

// Event Listener:  get breed info from submitted user input
$("#search-form").submit(function (e) {
  e.preventDefault();
  var userInput = e.target[0].value;

  if (breedList.includes(userInput)) {
    const breed = e.target[0].value;
    e.target[0].value = "";
    getBreedInfo(breed);
  } else {
    alert("No breed found with the name: " + userInput);
  }
});

// Event Listener:  get back to all breed page
backButton.addEventListener("click", getAllBreeds);

// Invoke functions
reset(true);
getAllBreeds();
